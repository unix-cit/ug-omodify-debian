#!/bin/bash
#

progname="host-rename"
ohost="lunipoulpetst1"
nhost="lunipoulpetst4"
oipv4="10.194.11.94"
nipv4="10.194.11.97"

################################################################################
# C B R   LIB
_B="\e[34m"
_R="\e[31m"
_G="\e[32m"
_N="\e[0m"
function pb()
{
    echo -e "${_B}$1${_N}"
}
function pr()
{
    echo -e "${_R}$1${_N}"
}
function pg()
{
    echo -e "${_G}$1${_N}"
}
function pbr()
{
    echo -e "${_B}$1 : ${_R}✘${_N}"
}
function pbg()
{
    echo -e "${_B}$1 : ${_G}✔${_N}"
}
function tab()
{
    if [[ $1 -eq 0 ]]; then
        cat -
    fi
    t="$(printf %${1}s)"
    sed "s|^|${t}|"
}
################################################################################

function change_hostname
{
    #hostname
    pb "/etc/hostname"
    if grep -qw "$ohost" /etc/hostname; then
        pb " - change /etc/hostname"
        cp /etc/hostname{,.${progname}}
        sed -i "s|${ohost}|${nhost}|g" /etc/hostname
        pg "    - changed."
    else
        pg "  - no need"
    fi

    #hosts
    pb "/etc/hosts"
    if grep -qw "${ohost}" /etc/hosts; then
        cp /etc/hosts{,.${progname}}
        sed -i "s|${ohost}|${nhost}|g" /etc/hosts
        pg "  - changed."
    else
        pg "  - no need"
    fi

    # hostname
    pb "hostname"
    if hostname | grep -qw "${ohost}"; then
        pb " - change hostname (hostname ${nhost})"
        hostname ${nhost}
        pg "   - Done !"
    else
        pg "  - no need"
    fi

    #mailname
    pb "/etc/mailname"
    if grep -qw "${ohost}" /etc/mailname; then
        cp /etc/mailname{,.${progname}}
        sed -i "s|${ohost}|${nhost}|g" /etc/mailname
        pg "  - changed."
    else
        pg "  - no need"
    fi

    #minion
    pb "/etc/salt/minion_id"
    has_changed_minion_id="False"
    if grep -qw "${ohost}" /etc/salt/minion_id; then
        cp /etc/salt/minion_id{,.${progname}}
        sed -i "s|${ohost}|${nhost}|g" /etc/salt/minion_id
        has_changed_minion_id="True"
        pg "  - changed."
    else
        pg "  - no need"
    fi

    if [[ ${has_changed_minion_id} = "True" ]]; then
        pb "The minion_id has changed:"
        pb " - Regenerate the pki"
        pb "   - remove /etc/salt/pki"
        rm -fr /etc/salt/pki
        pg "       Done !"
        pb "   - recreate them (systemctl restart salt-minion)"
        systemctl restart salt-minion | tab 7
        pg "       Done !"
    fi

    # ssh
    pb "/etc/ssh/ssh_host_*"
    if grep -qw "${ohost}" /etc/ssh/ssh_host* ; then
        pb " - remove /etc/ssh/ssh_host_*"
        rm /etc/ssh/ssh_host_*
        pb " - regenerate the ssh_host_*"
        dpkg-reconfigure openssh-server 2>&1 | tab 5
        pg "  - Done !"
    else
        pb "  - no need"
    fi

    pb "/root/.ssh/id_rsa*"
    if grep -qw "${ohost}" /root/.ssh/id_rsa* 2> /dev/null ; then
        pb " - remove /root/.ssh/id_rsa*"
        rm /root/.ssh/id_rsa*
        pb " - regenerate the /root/id_rsa*"
        ssh-keygen -t rsa -N "" -f /root/.ssh/id_rsa | tab 5
        pg "  - Done !"
    else
        pb "  - no need"
    fi
    
    pb "Other change to do:"
    pb ' - xymon with the command:'
    pb "for f in \$(grep -lE '\s${ohost}\.' /etc/xymon/hosts.d/*); do"
    pb "   echo \$f"
    pb "   sed -iE 's|(\s)${ohost}\.|\1${nhost}.|' \$f"
    pb "done"
    pb " - /etc/motd"
    pb " - salt with the commands:"
    pb "saltk-key -d ${nhost}"
    pb "saltk-key -a ${ohost}"
    pb "mv /srv/salt/pillar/minion_id/{${ohost},${nhost}}.sls"
    pb " - sauron"
    pb " - infoblox"
    pb " - change the tsm node"
}

function change_ipv4
{
    # ipv4
    pb "ipv4 on /etc/network/interfaces"
    if grep -w address /etc/network/interfaces 2> /dev/null | grep -qw ${oipv4}; then
        pb " - change address"
        sed -i "s|${oipv4}|${nipv4}|" /etc/network/interfaces
        pg "   - Done !"
    else
        pg " - no need"
    fi

    pb "ipv4 on /etc/network/interfaces.d/main4.conf"
    if grep -w address /etc/network/interfaces.d/main4.conf 2> /dev/null | grep -qw ${oipv4} ; then
        pb " - change address"
        sed -i "s|${oipv4}|${nipv4}|" /etc/network/interfaces.d/main4.conf
        pg "   - Done !"
    else
        pg " - no need"
    fi

    pb "enforce new ipv4"
    cur_ipv4=$(ip a | grep -w inet| grep -vw inet6 | grep -vw lo \
              | grep -v 10.194.11.95 | cut -d "/" -f1 | sed "s|.*inet ||g")
    need_reboot="False"
    if [[ ${cur_ipv4} != "${nipv4}" ]]; then
        pr " - Not enforced, reboot the server to enable it."
        pb "   the new ip will be (${nipv4})"
        need_reboot="True"
    else
        pg " - no need, already enforced !"
    fi

    pb "ip public or private"
    ipv4=${nipv4}
    if [[ ${ipv4/%\.*/} == 129 ]]; then
        is_public="True"
        pb " - public"
    else
        is_public="False"
        pb " - private"
    fi
}

function change_ipv6
{
    # THIS IS NOT TESTED, AND THIS NOT REALLY WRITTEN
    pb "ipv6"
    pb " - disable /etc/network/interfaces.d/main6.conf"
    if [[ -f /etc/network/interfaces.d/main6.conf ]]; then
        if [[ -x /sbin/ifdown ]]; then
            ifdown eth0
            pg "   - done"
        else
            pg "   - no need, as the cmd (ifdown) does not exist"
        fi
        mv /etc/network/interfaces.d/main6.conf{,.disabled}
    fi
    
    pb " - create tmp ipv6"
    cat > /etc/network/interfaces.d/tmp6.conf << EOF
    iface eth0 inet6 auto
EOF
    ifup eth0
    pb " - ifdown eth0; ifup eth0"

}

function choose_to_reboot
{
    if [[ ${needs_reboot} = "True" ]]; then
        pb "node needs to reboot."
        read -p "   Reboot it [y/N]"  do_reboot
        if [[ ${do_reboot} = "y" ]]; then
            pg "reboot ..."
            init 6
        fi
    fi
}

function create_snap
{
    if [[ ! -x /usr/local/bin/ug-vmware ]]; then
        pb "No ug-vmware presents. Do you still want to rename it ?"
        read -p "   Continue [y/N]"  do_continue
        if [[ ${do_reboot} = "n" ]]; then
            pg "exit ..."
            exit
        fi
    fi
    if ! ug-vmware snap create "b4 ug-omodify" "by briner"
    then
        pr "  - Unable to do a snapshot !"
        pb "  - Exit !"
        exit 1
    fi | tab 2
}

function usage
{
cat << EOF
Usage:
    hostname <old> <new>
    ipv4 <old> <new>
EOF
}
do_host=false
do_ipv4=false

case "$1" in
    hostname)
        create_snap
        shift
        do_host=true
        ohost=$1
        nhost=$2
        do_host=true
        change_hostname $ohost $nhost
        shift
        shift
        choose_to_reboot
        ;;
    ipv4)
        create_snap
        shift
        oipv4=$1
        nipv4=$2
        shift
        shift
        change_ipv4
        choose_to_reboot
        ;;
    *)
        usage
        exit 0
        ;;
esac        

if [[ $do_host == "true" ]]; then
    change_hostname
fi

if [[ $do_ipv4 == "true" ]]; then
    change_ipv4
fi

